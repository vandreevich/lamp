from setuptools import setup, find_packages

setup(
    name='lamp',
    version='0.0.1',
    description='Lamp',
    install_requires=(
        'tornado==4.3'
    ),
    entry_points = {
        'console_scripts': ['lamp=src.server:main'],
    },
    packages=find_packages(exclude=('venv','tests')),
    zip_safe=True,
)
