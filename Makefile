clean:
	rm -rf build
	rm -rf dist

export: clean
	git archive --format=tar HEAD | (mkdir -p build/ && cd build && tar xf -)
	mkdir -p dist

build: build-python-package

build-python-package: export
	cd build && \
		python setup.py --quiet sdist --dist-dir ../dist


all: build-python-package
