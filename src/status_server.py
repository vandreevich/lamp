#coding: utf8

# списал отсюда
# https://gist.github.com/mivade/d474e0540036d873047f

import signal
from tornado import web, gen
from tornado.options import options
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop, PeriodicCallback
from tornado.iostream import StreamClosedError
from tornado.queues import Queue, QueueEmpty

from lamp import lamp



html = '''
<html>
<head><title>lamp</title>
<style type="text/css">
#color {{
	width: 300px;
	height: 300px;
	border: 1px solid black;
	background-color: {};
}}
</style>
</head>
<body>
<h1>lamp</h1>
<div id="color"></div>
<script type="text/javascript">
    var source = new EventSource('/status/');
	source.onmessage = function(message) {{
	    var div = document.getElementById("color");
	    div.style.backgroundColor = message.data;
    }};
</script>
</body>
</html>
'''

class EventSource(web.RequestHandler):
    """Basic handler for server-sent events."""
    def initialize(self, queue):
        self._queue = queue
        self.set_header('content-type', 'text/event-stream')
        self.set_header('cache-control', 'no-cache')

    @gen.coroutine
    def publish(self, data):
        """Pushes data to a listener."""
        try:
            self.write('data: {}\n\n'.format(data))
            yield self.flush()
        except StreamClosedError:
            pass

    @gen.coroutine
    def get(self):
        while True:
            if self._queue.qsize():
                try:
                    yield self.publish(self._queue.get_nowait())
                except QueueEmpty:
                    continue
            else:
                yield gen.sleep(0.005)


class MainHandler(web.RequestHandler):
    def get(self):
        color = 'black'
        if lamp.state == lamp.ON:
            color = lamp.css_color
        self.write(html.format(color))
