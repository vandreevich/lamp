#coding: utf8

import struct

import signal
from tornado import ioloop, tcpserver, gen, web
from tornado.queues import Queue, QueueFull
from tornado.httpserver import HTTPServer
from tornado.ioloop import PeriodicCallback

import lamp
import status_server


class CommandDispatcher(tcpserver.TCPServer):
    def __init__(self, *args, **kwargs):
        que = kwargs.pop('queue')
        super(CommandDispatcher, self).__init__(*args, **kwargs)
        self._queue = que

    @gen.coroutine
    def handle_stream(self, stream, address):
        while 1:
            line = yield stream.read_bytes(3)
            if line:
                try:
                    c_type, length = struct.unpack('!Bh', line)
                except ValueError:
                    continue

                value = None
                if length > 0:
                    line = yield stream.read_bytes(length)
                    try:
                        value = struct.unpack('!{}B'.format(length), line)
                    except ValueError:
                        continue

                try:
                    cmd = lamp.COMMANDS[int(c_type)]
                    if value is not None:
                        cmd(value)
                    else:
                        cmd()
                except KeyError:
                    print 'Warning: unknown command type: {}'.format(hex(c_type))
                    continue

                # оповецение клиента(браузера)
                # для одного соединения хватит и такой схемы
                if int(c_type) == lamp.TURN_OFF:
                    color = 'black'
                else:
                    color = lamp.lamp.css_color

                try:
                    self._queue.put_nowait(color)
                except QueueFull:
                    continue
                ######################
            else:
                yield gen.sleep(0.05)


def main():
    _queue = Queue(maxsize=255)

    cmd_server = CommandDispatcher(queue=_queue)
    cmd_server.listen(9999)

    app = web.Application(
        [
            (r'/', status_server.MainHandler),
            (r'/status/', status_server.EventSource, dict(queue=_queue))
        ],
        debug=True
    )
    server = HTTPServer(app)
    server.listen(8080)

    signal.signal(signal.SIGINT, lambda x, y: ioloop.IOLoop.instance().stop())
    print 'Starting..'
    print 'Command server listening 9999'
    print 'Status server listening 8080'
    ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    main()
