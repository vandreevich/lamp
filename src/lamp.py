#coding: utf8

class Lamp(object):
    ON = 1
    OFF = 0

    def __init__(self):
        self._state = Lamp.OFF
        self._color = (255, 255, 255)

    def on(self):
        if self._state == Lamp.OFF:
            self._state = Lamp.ON
            print 'The lamp is on'

    def off(self):
        if self._state == Lamp.ON:
            self._state = Lamp.OFF
            print 'The lamp is off'

    def change_color(self, value):
        try:
            value = tuple([int(i) for i in value])
        except (TypeError, ValueError):
            print 'bad color value'
            return

        self._color = value
        print 'Color was changed to {}'.format(str(value))

    @property
    def state(self):
        return self._state

    @property
    def color(self):
        return str(self._color)

    @property
    def css_color(self):
        return 'rgb' + self.color



lamp = Lamp()

TURN_ON = 0x12
TURN_OFF = 0x13
CHANGE_COLOR = 0x20

# один фонарь, сохраним его в замыкании
COMMANDS = {
    TURN_ON: lambda : lamp.on(),
    TURN_OFF: lambda : lamp.off(),
    CHANGE_COLOR: lambda value :lamp.change_color(value),
}
