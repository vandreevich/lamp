#coding: utf8

import unittest

from src import lamp

class TestLamp(unittest.TestCase):
    def setUp(self):
        self._lamp = lamp.lamp

    def test_turn_on(self):
        self._lamp.on()
        self.assertEqual(self._lamp.state, self._lamp.ON)

    def test_turn_of(self):
        self._lamp._state = self._lamp.ON
        self._lamp.off()
        self.assertEqual(self._lamp.state, self._lamp.OFF)

    def test_color(self):
        value = (255, 0, 255)
        self._lamp.change_color(value)
        self.assertEqual(self._lamp._color, value)

    def test_bad_color(self):
        value = (255, 0, 'adsf')
        color = (1, 1, 1)
        self._lamp._color = color
        self._lamp.change_color(value)
        self.assertEqual(self._lamp._color, color)



if __name__ == '__main__':
    unittest.main()
